package org.alehsidliarevich.sandbox.resttest.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Value;
import lombok.extern.jackson.Jacksonized;

@Value
@Jacksonized
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class Entry {
    @JsonProperty("API")
    String api;
    @JsonProperty("Description")
    String description;
    @JsonProperty("Auth")
    String auth;
    @JsonProperty("HTTPS")
    Boolean https;
    @JsonProperty("Cors")
    String cors;
    @JsonProperty("Link")
    String link;
    @JsonProperty("Category")
    String category;
}