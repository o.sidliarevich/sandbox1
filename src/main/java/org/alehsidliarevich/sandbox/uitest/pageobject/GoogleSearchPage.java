package org.alehsidliarevich.sandbox.uitest.pageobject;

import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Selenide.page;

public class GoogleSearchPage {
    public GoogleResultsPage search(String query) {
        $(By.name("q")).setValue(query).pressEnter();
        return page(GoogleResultsPage.class);
    }

    public GoogleSearchPage acceptAllCookies(){
        $(By.id("L2AGLb")).click();
        return this;
    }
}
