package org.alehsidliarevich.sandbox.uitest.pageobject.model;

import lombok.Value;

@Value
public class SearchResult {
    String text;
    String link;
}
