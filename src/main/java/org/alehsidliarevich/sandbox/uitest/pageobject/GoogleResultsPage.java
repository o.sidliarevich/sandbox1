package org.alehsidliarevich.sandbox.uitest.pageobject;

import org.alehsidliarevich.sandbox.uitest.pageobject.model.SearchResult;

import java.util.List;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$$x;
import static java.util.stream.Collectors.toList;

public class GoogleResultsPage {

    private static final String SEARCH_RESULTS_XPATH = "//div[@class='yuRUbf']/a";

    public List<SearchResult> results() {
        var results = $$x(SEARCH_RESULTS_XPATH).filter(visible);
        return results.stream().map(r -> new SearchResult(r.$x("h3").getOwnText(), r.getAttribute("href"))).collect(toList());
    }
}
