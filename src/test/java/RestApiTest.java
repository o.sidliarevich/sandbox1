import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j;
import org.alehsidliarevich.sandbox.resttest.model.Entries;
import org.testng.annotations.Test;

import java.net.URI;
import java.net.URLEncoder;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;

import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;

@Log4j
public class RestApiTest {

    private static final String API_URL = "https://api.publicapis.org/entries";
    private static final String CATEGORY_NAME = "category";
    private static final String CATEGORY = "Authentication & Authorization";
    private static final String CATEGORY_QUERY_PARAM = "Authentication%20&%20Authorization";

    //it was not clear what should I use as expected values, so I decided to compare values from "base" API request and
    // request with query param
    @Test
    @SneakyThrows
    public void restTest() {
        var allEntries = getEntriesFromApiEndpoint(API_URL);
        performBasicEntriesValidation(allEntries);

        var entriesListWithCategoryFromGeneralRequest
                = allEntries.getEntries().stream().filter(e -> Objects.equals(CATEGORY, e.getCategory())).collect(toList());

        var entriesWithQuery = getEntriesFromApiEndpoint(API_URL + "?" + CATEGORY_NAME + "=" + URLEncoder.encode(CATEGORY, "UTF-8"));
        performBasicEntriesValidation(entriesWithQuery);

        var entriesListFromQueryRequest = entriesWithQuery.getEntries();

        assertThat(entriesListFromQueryRequest.size())
                .as("Entries lists sizes are not equals")
                .isEqualTo(entriesListWithCategoryFromGeneralRequest.size());

        assertThat(entriesListFromQueryRequest)
                .as("Entries lists elements are not equals")
                .containsExactlyInAnyOrderElementsOf(entriesListWithCategoryFromGeneralRequest);
    }

    @SneakyThrows
    private static Entries getEntriesFromApiEndpoint(String apiUrl) {
        HttpRequest request = HttpRequest.newBuilder().GET().uri(URI.create(apiUrl)).build();

        CompletableFuture<Entries> entriesFuture = HttpClient.newHttpClient()
                .sendAsync(request, HttpResponse.BodyHandlers.ofString())
                .thenApply(HttpResponse::body)
                .thenApply(body -> parseJsonToEntity(body, Entries.class));

        return entriesFuture.get();
    }

    private static void performBasicEntriesValidation(Entries entries) {
        assertThat(entries.getEntries()).as("Response has no field Entries").isNotNull();
        assertThat(entries.getEntries()).as("Response has empty Entries list").isNotEmpty();
        assertThat(entries.getCount()).as("Response has no field Count").isNotNull();
        assertThat(entries.getCount()).as("Response field Count value is not equals to Entries list size")
                .isEqualTo(entries.getEntries().size());
        entries.getEntries().forEach(e -> log.debug(e.getLink()));
    }

    @SneakyThrows
    private static <T> T parseJsonToEntity(String json, Class<T> typeParameterClass) {
        return new ObjectMapper().readValue(json, typeParameterClass);
    }
}
