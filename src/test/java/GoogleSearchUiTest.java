import lombok.extern.log4j.Log4j;
import org.alehsidliarevich.sandbox.uitest.pageobject.GoogleSearchPage;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.open;
import static org.assertj.core.api.Assertions.assertThat;

@Log4j
public class GoogleSearchUiTest {

    private static final String GOOGLE_BASE_PAGE_URL = "https://www.google.co.il/";
    private static final String SEARCH_STRING = "Java";
    private static final String FIRST_RESULT_TEXT_TO_CONTAINS = "Java";
    private static final String LAST_RESULT_TEXT_TO_CONTAINS = "Interview";

    @Test
    public void googleSearchTest() {
        var searchPage = open(GOOGLE_BASE_PAGE_URL, GoogleSearchPage.class);
        var resultsPage = searchPage.acceptAllCookies().search(SEARCH_STRING);
        var results = resultsPage.results();
        assertThat(results).as("Search didn't return any results").isNotEmpty();
        results.forEach(r -> log.debug(r.getLink()));

        var firstResult = results.get(0);
        var lastResult = results.get(results.size() - 1);

        assertThat(firstResult.getText())
                .as("First search result text should contains: %s", FIRST_RESULT_TEXT_TO_CONTAINS)
                .containsIgnoringCase(FIRST_RESULT_TEXT_TO_CONTAINS);

        assertThat(lastResult.getText())
                .as("Last search result text should contains: %s", LAST_RESULT_TEXT_TO_CONTAINS)
                .doesNotContainIgnoringCase(LAST_RESULT_TEXT_TO_CONTAINS);
    }
}